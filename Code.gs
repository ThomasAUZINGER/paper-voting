/**
 * PAPER VOTING
 * by Thomas AUZINGER <thomas@auzinger.name>
 *
 * See https://gitlab.com/ThomasAUZINGER/paper-voting/ for documentation and licensing terms.
 */

// Limit the required permissions to the corresponding spreadsheet only.
/**
 * @OnlyCurrentDoc
 */

var VOTING_TEMPLATE_NAME = "VOTING TEMPLATE - DO NOT EDIT";
var RESULT_TEMPLATE_NAME = "RESULT TEMPLATE - DO NOT EDIT";
var VOTING_SHEET_NAME_PREFIX = "VOTING - ";
var RESULT_SHEET_NAME_PREFIX = "RESULT - ";
var FIRST_PAPER_TITLE_ROW = 11; // Must correspond to the first empty row in the voting sheet.
var QUERY_ROW = 4; // Must correspond to the first empty row in the result sheet.
var VOTES_COLUMN = 1; // Must correspond to the associated column in the result sheet.
var PAPER_TITLES_COLUMN = 2; // ---||---
var VOTES_NUMBER_COLUMN = 8; // ---||---
var VOTERS_COLUMN = 9; // ---||---

/**
 * Add a menu item for voting sheets creation.
 * This function does not need to be explicity called.
 */
function onOpen() {
  SpreadsheetApp.getUi()
    .createMenu("Voting")
    .addItem("Create Sheets", "createVotingSheets")
    .addToUi();
}

/**
 * Create the voting sheets.
 */
function createVotingSheets() {
  var app = SpreadsheetApp.getActiveSpreadsheet();
  var ui = SpreadsheetApp.getUi();
  
  verifyScript();
  var sheets = initializeSheets();
  var votingSheet = sheets[0];
  var resultSheet = sheets[1];
  addPaperInfo(votingSheet);
  addQueries(votingSheet, resultSheet);

  ui.alert("Finished", "The voting and result tables were created.", ui.ButtonSet.OK);
}

/**
 * Verify that the current script corresponds to the latest version.
 *
 * @throws Will throw an error if the latest version can not be fetched from GitLab.
 * @throws Will throw an error if the version do not match and the user does not ignore this fact.
 */
function verifyScript() {
  var ui = SpreadsheetApp.getUi();
  
  // This function loads the most recent script code from the development resource at GitLab and
  // compares it with this script. A warning is issued if the code does not correspond to the latest
  // version.
  var repositoryUrl = "https://gitlab.com/ThomasAUZINGER/paper-voting";
  var scriptUrl = repositoryUrl + "/raw/master/Code.gs";
  
  // Fetch the source information from the supplied url.
  try {
    var response = UrlFetchApp.fetch(scriptUrl);
    var source = response.getContentText();
  } catch(err) {
    throw new Error("Failed to load latest script version from GitLab: " + err + ".");
  }
  
  var thisScript = ScriptApp.getResource("Code").getDataAsString();
  var isLatestVersion = thisScript === source;
  if (!isLatestVersion) {
    var response = ui.alert(
      "Version mismatch",
      "This is not the latest version of this program (or the program was altered).\nSee " +
        repositoryUrl + " for the latest version.\nRun anyhow (which is not recommended)?",
      ui.ButtonSet.YES_NO);
    if (response != ui.Button.YES) {
      throw new Error("Version mismatch detected.");
    }
  }
}

/**
 * Initialize the voting sheets.
 * This function prompt the user for a venue name and subsequently creates two sheets. One for the
 * voting process and one for the result summary. The title of both sheets is appended with the
 * given venue name.
 *
 * @returns {Array} An array of length 2 containing the generated sheets.
 * @throws Will throw an error if sheets with the chosen name already exist.
 */
function initializeSheets() {
  var app = SpreadsheetApp.getActiveSpreadsheet();
  var ui = SpreadsheetApp.getUi();
  
  // Obtain venue name from user.
  var result = ui.prompt(
    "Enter conference NAME", "e.g., Siggraph 2018 or Siggraph Asia 2018", ui.ButtonSet.OK);
  if (result.getSelectedButton() != ui.Button.OK) {
    ui.alert("No name supplied. Aborting...");
    return;
  }
  var name = result.getResponseText();
  
  if (app.getSheetByName("RESULT - " + name)) {
    ui.alert('A sheet with name "' + "RESULT - " + name + '" already exists. Choose a different' +
             ' name or delete this sheet.',
             ui.ButtonSet.OK);
    throw new Error("Duplicate sheets not allowed.");
  }
  if (app.getSheetByName("VOTING - " + name)) {
    ui.alert('A sheet with name "' + "VOTING - " + name + '" already exists. Choose a different' +
             ' name or delete this sheet.',
             ui.ButtonSet.OK);
    throw new Error("Duplicate sheets not allowed.");
  }
  
  // Create sheet for voting results.
  var resultSheetTemplate = app.getSheetByName(RESULT_TEMPLATE_NAME);
  var resultSheet = app.insertSheet("RESULT - " + name, 0, {template: resultSheetTemplate});
  resultSheet.setName(RESULT_SHEET_NAME_PREFIX + name); // Add venue name to sheet name.
  // Add venue name to title.
  resultSheet.getRange(1, 1).setValue(resultSheet.getRange(1, 1).getValue() + name);
  
  // Create sheet for voting.
  var votingSheetTemplate = app.getSheetByName(VOTING_TEMPLATE_NAME);
  var votingSheet = app.insertSheet("VOTING - " + name, 0, {template: votingSheetTemplate});
  votingSheet.setName(VOTING_SHEET_NAME_PREFIX + name); // Add venue name to sheet name.
  // Add venue name to title.
  votingSheet.getRange(1, 1).setValue(votingSheet.getRange(1, 1).getValue() + name);
  
  return [votingSheet, resultSheet];
}

/**
 * Add the venue's paper titles (and links to them) to the voting sheet.
 * This function prompts the user for a URL of a webpage by Ke-Sen Huang that contains the venue's
 * paper list. The source of the webpage is parsed with a regular expression, which extracts the
 * paper titles. Optional links to the papers' project pages are added if they are found.
 *
 * @param {Sheet} votingSheet - The voting sheet generated from the voting sheet template.
 * @throws Will throw an error if the page source can be loaded from the supplied URL.
 */
function addPaperInfo(votingSheet) {
  var ui = SpreadsheetApp.getUi();
  
  // Obtain paper list url from user.
  var result = ui.prompt("Enter Ke-Sen URL",
                         "e.g., http://kesen.realtimerendering.com/sig2018.html",
                         ui.ButtonSet.OK);
  if (result.getSelectedButton() != ui.Button.OK) {
    ui.alert("No URL supplied. Aborting...");
    return;
  }
  var url = result.getResponseText();
  
  // Fetch the source information from the supplied url.
  try {
    var response = UrlFetchApp.fetch(url);
    var source = response.getContentText();
  } catch(err) {
    throw new Error("Failed to load the page source from the given URL: " + err + ".");
  }
  
  // Extract individual paper titles and urls.
  // See https://regex101.com/r/pIDDll/1 for an explanation.
  var regex = /<dt><B>(.+?)<\/B>\n+(<a href="(.+?)")?/g;
  var row = FIRST_PAPER_TITLE_ROW;
  var result;
  while(result = regex.exec(source)) {
    if (result[3] === undefined) {
      votingSheet.getRange(row, 1).setValue(result[1]); // Add paper title without hyperlink.
    } else {
      // Add paper title with hyperlink.
      votingSheet.getRange(row, 1).setValue('=HYPERLINK("' + result[3] + '","' + result[1] + '")');
    }
    row += 1;
  }
}

/**
 * Add queries that generate the voting results.
 * This function adds queries using the Google Visualization API Query Language that summarize the
 * results of the voting process.
 *
 * @param {Sheet} votingSheet - The voting sheet generated from the voting sheet template.
 * @param {Sheet} resultSheet - The result sheet generated from the result sheet template.
 */
function addQueries(votingSheet, resultSheet) {
  var name = votingSheet.getSheetName();
  // Set queries for the results.
  resultSheet.getRange(QUERY_ROW, VOTES_COLUMN).setValue(
    "=QUERY('" + name + "'!A" + FIRST_PAPER_TITLE_ROW + ":C500,\"select (C) order by (C) desc\")");
  resultSheet.getRange(QUERY_ROW, PAPER_TITLES_COLUMN).setValue(
    "=QUERY('" + name + "'!A" + FIRST_PAPER_TITLE_ROW + ":C500,\"select (A) order by (C) desc\")");
  resultSheet.getRange(QUERY_ROW, VOTES_NUMBER_COLUMN).setValue(
    "=QUERY(transpose('" + name + "'!E8:Z9), \"select Col2 order by Col2 desc\")");
  resultSheet.getRange(QUERY_ROW, VOTERS_COLUMN).setValue(
    "=QUERY(transpose('" + name + "'!E8:Z9), \"select Col1 order by Col2 desc\")");
}
