# Paper Voting 1.0

by Thomas AUZINGER  
<thomas@auzinger.name>

## Description

Based on [Ke-Sen Huang's collection](http://kesen.realtimerendering.com/) of Computer Graphics publications, this program creates voting tables to rank papers of a specified venue.
The original purpose of this program is to create suggestions for academic reading groups, i.e., to determine which papers are of interest to the participants of the group.

This program is realized as a [*Google Spreadsheet App*](https://www.google.com/sheets/about/).
Thus, the organizer of the vote needs to have a [*Google Account*](https://myaccount.google.com) to create a copy of the spreadsheet.
Voters, however, do *not* need such an account.

## Quick Start

Create a vote for your reading group in less than 5 minutes:

1. Open [link](https://docs.google.com/spreadsheets/d/1STyaZkCoFltCmYmmk2Jy627GCuCtQlQBUtydW6A8iaI/copy) and copy the spreadsheet.
2. Reload the spreadsheet by pressing F5 and wait until the `Voting` menu shows up.<br>Repeat pressing F5 if it does not show up after a few seconds.
3. Give the script all necessary permissions, follow the dialogs, and share the newly created voting sheets with all voters.
4. Vote!

In case you have questions or encounter errors, please consult the [detailed installation description](#detailed-installation) below.

## Bugs and Suggestions

Please use the [GitLab issue tracker](https://gitlab.com/ThomasAUZINGER/paper-voting/issues) to report bugs or suggest features.

## Detailed Installation

1. Log into your *Google Account*.
2. Create a copy of `Paper Voting` by using this [link](https://docs.google.com/spreadsheets/d/1STyaZkCoFltCmYmmk2Jy627GCuCtQlQBUtydW6A8iaI/copy).  
   Following this link should open a dialog box that asks for a confirmation of the copying process.  
   The copy is created in the root folder of the *Google Drive* associated with your account.
3. Share your copy of `Paper Voting` with all potential voters.  
   This can be done by creating a shareable link that permits editing the spreadsheet (consult the [official Google Support](https://support.google.com/drive/answer/2494822#view_comment_edit) for details).
4. The `Paper Voting` spreadsheet should consist of a single visible Sheet, which is named `README`.  
   The instruction in the `Usage` section describe on how to create the voting sheets for a chosen venue from Ke-Sen Huang's webpage.  
   It is possible that the webpage of the `README` sheet needs to be reloaded several times before the `Voting` menu shows up at the top menu.  
   The script requires several permissions to be able to run (e.g., edit the spreadsheet, load external webpages, etc.).

## Development & Contribution

This program is realized as a Google Spreadsheet App and consists of four components:

1. A sheet named `README`.  
   It is the documentation that you are currently reading.
2. A hidden sheet named `VOTING TEMPLATE - DO NOT EDIT` which serves as a template for the voting sheets.  
   It can be selected via the menu item `View → Hidden sheets → ...` from the top of this page.
3. A hidden sheet named `RESULT TEMPLATE - DO NOT EDIT` which serves as a template for the result sheets.  
   It can be selected via the menu item `View → Hidden sheets → ...` from the top of this page.
4. A script file utilizing the Google Apps Script scripting language that contains the main functionality for the creation of voting and result sheets.  
   In can be edited via the menu item `Tools → Script editor`.  
   The project of the script file is named `Create Voting Sheets` and it is written in Javascript.  

If you want to contribute to this project, you can suggest a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) after [forking](https://docs.gitlab.com/ee/user/project/merge_requests/allow_collaboration.html) this project. Note that this only applies to the script and documentation.  
Alternatively, you can drop me an email at <thomas@auzinger.name> and we figure something out. This is also the required route to alter the template sheets of the project.

## License

Copyright 2018- Thomas AUZINGER <thomas@auzinger.name>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

MIT License from https://opensource.org/licenses/MIT.
